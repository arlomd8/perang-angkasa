﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public int bulletSpeed;
    private float naik;
    public Transform explodeLoc;
    void Update()
    {
        naik = bulletSpeed * Time.deltaTime;
        transform.Translate(Vector3.up * naik);

        if (transform.position.y > 5.5)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle"))
        {
            PlayerScript.playerScore += 10;
            other.gameObject.transform.position = new Vector3(Random.Range(-2.8f, 2.8f), 5.5f, transform.position.z);
            var explodeLocation = Instantiate(explodeLoc, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
