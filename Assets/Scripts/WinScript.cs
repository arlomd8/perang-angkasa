﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScript : MonoBehaviour
{
    private void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 200, 50), "KAMU MENANG, COBA LAGI ?"))
        {
            PlayerScript.playerScore = 0;
            Application.LoadLevel("SceneAwal");
        }
    }
}
