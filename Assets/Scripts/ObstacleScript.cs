﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{
    public int obsSpeed;
    private float turun;
    void Start()
    {
        transform.position = new Vector3(0, 5.5f, transform.position.z);
    }

    void Update()
    {
        turun = obsSpeed * Time.deltaTime;
        transform.Translate(Vector3.up * turun);
        if (transform.position.y < -5.5)
        {
            float randX = Random.Range(-2.8f, 2.8f);
            transform.position = new Vector3(randX, 5.5f, transform.position.z);
        }

    }
}
