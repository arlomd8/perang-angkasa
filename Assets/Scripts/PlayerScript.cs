﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public int playerSpeed;
    private float geser, naik, batasKanan;
    public int playerLives;
    public static int playerScore;
    public Rigidbody bullet;
    private float waitTime = 5f;
    
    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 200, 50), "Skor : " + playerScore);
        GUI.Label(new Rect(10, 30, 200, 50), "Nyawa : " + playerLives);

    }

    void Update()
    {
        geser = (playerSpeed * Input.GetAxis("Horizontal")) * Time.deltaTime;
        transform.Translate(Vector3.right * geser);
        naik = (playerSpeed * Input.GetAxis("Vertical")) * Time.deltaTime;
        transform.Translate(Vector3.up * naik);

        if (transform.position.x > 2.8)
        {
            transform.position = new Vector3(2.8f, transform.position.y, transform.position.z);
        }

        if (transform.position.x < -2.8)
        {
            transform.position = new Vector3(-2.8f, transform.position.y, transform.position.z);
        }

        if (transform.position.y > 5)
        {
            transform.position = new Vector3(transform.position.x, 5f, transform.position.z);
        }

        if (transform.position.y < -5)
        {
            transform.position = new Vector3(transform.position.x, -5f, transform.position.z);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            var shootBullet = Instantiate(bullet, transform.position, transform.rotation);
        }

        if (playerLives <= 0)
        {
            playerLives = 0;
            Application.LoadLevel("SceneKalah");
        }

        if (playerScore >= 250)
        {
            Application.LoadLevel("SceneMenang");
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (playerScore % 50 == 0)
            {
                gameObject.GetComponent<MeshCollider>().isTrigger = false;
                StartCoroutine(Invincible(waitTime));
            }
            if(playerScore % 50 != 0)
            {
                gameObject.GetComponent<MeshCollider>().isTrigger = true;
            }
        }
    }

    private IEnumerator Invincible(float waktu)
    {
        yield return new WaitForSeconds(waktu);

        gameObject.GetComponent<MeshCollider>().isTrigger = true;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle"))
        {
            other.gameObject.transform.position = new Vector3(Random.Range(-2.8f, 2.8f), 5.5f, transform.position.z);
            playerLives--;
        }
    }
}
