﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScript : MonoBehaviour
{
    private void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 200, 50), "SELAMAT DATANG DI GAME PETUALANG ANGKASA");
        GUI.Label(new Rect(10, 50, 200, 50), "GUNAKAN W,A,S,D UNTUK BERGERAK, SPASI UNTUK MENEMBAK");
        GUI.Label(new Rect(10, 110, 200, 50), "TEKAN LEFT CTRL KETIKA SKOR MEMILIKI KELIPATAN 50 UNTUK MEMBUAT PESAWAT KEBAL SERANGAN SEALAM 5 DETIK");
        GUI.Label(new Rect(10, 160, 200, 50), "DAPATKAN SCORE 250 UNTUK MEMENANGKAN PERMAINAN");

        if (GUI.Button(new Rect(10, 210, 200, 50), "Main")){
            Application.LoadLevel("Level1");
        }
    }
}
